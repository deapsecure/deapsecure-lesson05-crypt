---
title: "Learning Resources for Python Programming"
---

This page curates a few resources for learning Python on your own.
The materials are mostly targeted to novice learners.
Choose one that fits your learning style (video-based,
text-based, or interactive web-based).

Additional pointers for people who have prior programmingexperience
will be added later and be marked by "ADVANCED:" prefix.



## NOVICE: Video-based Courses

#### "Python for Beginners" (from Microsoft)

<https://channel9.msdn.com/Series/Intro-to-Python-Development?WT.mc_id=python-c9-niner>

This is free online video course recently developed by Microsoft.
Courses are short (3-12 minutes each); there are a total of 44 videos.
Recommended episodes to watch first (if time is limited): 1, 2, 3, 5, 9, 13, 25, 27, 29, 38.
There are a lot of demo videos interspersed (e.g. video #6 is the demo for #5).
You can install Python on your own computer so that you can practice in your own time.

Video playlist: <https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6>



## NOVICE: Text-based Courses

#### "Plotting and Programming in Python" (from Software Carpentry)

<http://swcarpentry.github.io/python-novice-gapminder/>

If you prefer to read your materials (no videos), please consider this
introductory lesson from Software Carpentry.
This lesson is still being improved,
but it is a very good material as it stands. 
This lesson uses Pandas and DataFrame as the data-handling framework.
Pandas is excellent at handling heterogeneous, table-like data.
If in your work you are using homogenous, potentially
multi-dimensional arrays (grids, matrices, tensors), then I suggest
that you learn about Numpy array in the other Software Carpentry
lesson (see below).


#### "Programming with Python" (from Software Carpentry)
 
<http://swcarpentry.github.io/python-novice-inflammation/>

This is an alternative lesson that is using Numpy and ndarray as
the data-handling framework.
Many people in physical sciences and engineering may find this lesson
more familiar to them, because they deal more frequently with grids, matrices,
tensors in their computational tasks.



## NOVICE: Interactive Lessons

#### LearnPython.org

<https://www.learnpython.org/>

LearnPython has a nice web-based tutorial on Python.
It includes embedded Python environment which makes it a
zero-requirement learning environment.


#### Data Gymnasia (on Mathigon platform)

<https://mathigon.org/data-gymnasia>

Intro to Python starting lesson:
<https://mathigon.org/course/programming-in-python/introduction>

This lesson, produced by Brown Data Science Initiative,
is an all-in-one web-based learning environment on many topics
related to Data Science.
Not limited to Python, it also has linear algebra, calculus,
statistics, machine learning, utilities (UNIX, Git, LaTeX, etc), and
many more!



## Suggestions

1. Whichever lesson series you choose to take, you need to get your hands
   dirty with actual Python programming.
   We suggest installing [Anaconda Python distribution][anaconda-download]
   on your machine
   ([Windows install guide][anaconda-win-guide],
   [MacOS install guide][anaconda-mac-guide]).
   When downloading, select Python version >= 3.7.
   [Google Colab][google-colab] provides an alternative means
   using JupyterLab notebook, which requires no installation on your own machine.

2. Practice and frequent use are the key to mastering a programming language.
   There is no shortcut to this.

3. If this is your first time ever to learn programming,
   it is better to stick with *one* lesson plan, do as many hands-on activities
   as you can do, and finish that one, before jumping to other courses/tutorials.
   There is no perfect lesson material that fits everyone.
   But following through with one lesson material will allow you to build the
   fundamentals

[anaconda-download]: https://www.anaconda.com/distribution/#download-section
[anaconda-win-guide]: https://problemsolvingwithpython.com/01-Orientation/01.03-Installing-Anaconda-on-Windows/
[anaconda-mac-guide]: https://problemsolvingwithpython.com/01-Orientation/01.04-Installing-Anaconda-on-MacOS/
[google-colab]: https://colab.research.google.com/


{% include links.md %}

---
title: Summary
---

Here are the objectives of this training module:

 * Understanding encryption and the issues related to it

 * Understanding what homomorphic encryption is and what can be done with it

 * Practical familiarity with Paillier encryption as one class of
   homomorphic encryption

 * Practical experience with speeding up homomorphic encryption with
   parallel computation using MPI (Message Passing Interface).

Hope y'all enjoy the workshop today!

Please provide feedback to us: We care for every bit of input that you give.
We encourage you to leave written feedback so that we can improve this
workshop in the future.

{% comment %}

<https://odu.co1.qualtrics.com/jfe/form/SV_6DPvKACfvdWdrDf>
![POST-survey-qr-wksp-5.png](../fig/POST-survey-qr-wksp-5.png)

{% endcomment %}

{% comment %}
{% include carpentries.html %}
{% endcomment %}
{% include links.md %}

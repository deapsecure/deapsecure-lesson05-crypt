---
title: "Credits for materials used"
---

## Image credits

* ASCII code chart, by Wikimedia user Anomie:
  * URL: https://commons.wikimedia.org/wiki/File:ASCII_Code_Chart.svg
  * Direct download: <https://upload.wikimedia.org/wikipedia/commons/4/4f/ASCII_Code_Chart.svg>
  * Date retrieved: 2020-10-29

<!--
* ASCII table: Derived from `Ascii-codes-table.png`, by Yuriy Arabskyy

  * <https://commons.wikimedia.org/wiki/File:Ascii-codes-table.png>
  * <https://upload.wikimedia.org/wikipedia/commons/2/26/Ascii-codes-table.png>
-->

{% include links.md %}

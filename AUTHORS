AUTHORS
-------

This lesson module was initially furnished by the DeapSECURE Team
at Old Dominion University by the following contributors
(2018-2021):

  * Dr. Hongyi "Michael" Wu
  * Dr. Masha Sosonkina
  * Dr. Wirawan Purwanto
  * Issakar Doude
  * Rui Ning
  * Qiao Zhang
  * Liuwan Zhu
  * Yuming He
  * Jewel Ossom
  * Jacob Strother
  * Rosby Asiamah
  * Bahador Dodge

Contact us via email at: deapsecure@odu.edu .


ACKNOWLEDGMENTS
---------------

The development of the DeapSECURE training was funded by the
U.S. National Science Foundation through the CyberTraining grant
#1829771.

We gratefully acknowledge the support provided by the Old Dominion
University (ODU), the ODU School of Cybersecurity (formerly Center for
Cybersecurity Education and Research), the ODU Department of
Computational Modeling and Simulation Engineering, and the ODU
Research Computing Services, part of ODU Information Technology Services.

If you use and derive from our work, please cite the publications
listed in the CITATION file included in this repository
(https://gitlab.com/deapsecure/deapsecure-lesson05-crypt/-/blob/gh-pages/CITATION).

---
layout: reference
---

## Glossary

TODO


## Python Learning Resources

From Data Gymnasia:
<https://mathigon.org/course/programming-in-python/basics>

Cheat sheets (very useful to have while coding):
<https://ehmatthes.github.io/pcc/cheatsheets/README.html>


{% include links.md %}

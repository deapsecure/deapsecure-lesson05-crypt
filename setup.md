---
title: Setup
---

## Setting Up Files

### Hands-on Files on Turing

Hands-on files are located on Turing on this subdirectory:

    /scratch-lustre/DeapSECURE/module05/Exercises

Copy the entire directory tree to your `~/CItraining/module5`:

    $ mkdir -p ~/CItraining/module5

    $ cd ~/CItraining/module5

    $ cp -pr /scratch-lustre/DeapSECURE/module05/Exercises/. .

(Don't forget the last period!)


## Obtaining Compute Resource

In this workshop, we will begin by doing some interacting computation using
homomorphic encryption.
We will use one CPU core as a starting point.
Here's how we request the resource on Turing:

    $ salloc -c 1

Notice that the host name changed from `turing1` to
something like `coreV3-23-024` or `coreV4-21-010`.
That is an indicator that we have logged on to a compute node.

> ## Parallel computing?
>
> In the second half of the workshop there will be opportunity to do
> parallel computation to speed up the encryption tasks.
> No need to pre-allocate multiple cores; we will request those when
> submitting jobs via SLURM batch scheduler.
{: .callout}



## Setting Up Environment

After you obtain the compute resource, you will need to load a number of modules:

    enable_lmod

    module load icc/18 impi/18
    module load python/3.6
    module load mpi4py
    module load ipython
    module load numpy scipy
    module load matplotlib         # for image loading/saving later
    module load pandas             # in case needed for large dataset

    # dependencie of gmpy2 and phe
    module load gmp mpfr mpc

    module use /scratch-lustre/DeapSECURE/lmod
    module load DeapSECURE

A few modules are provided by DeapSECURE itself: `phe` (a.k.a. `python-paillier`),
`pycrypto`, and `gmpy2`.

We created a shell command file named `crypto-env-py3` in your hands-on
directory to ease reloading of these modules later on.
To take advantage of this, please issue this command in the same directory as before:

    $ source crypto-env-py3

Do this only once per shell session, right after you obtain your computing cores.


## Run IPython

Now start `ipython`:

    $ ipython

Your computing environment is ready!

{% include links.md %}

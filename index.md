---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---

<!-- not a nice hack to add vertical space, but it works -->
<br>

This lesson module introduces the **homomorphic encryption** technique,
which is powerful to address concerns around data privacy, when private data
has to be shared with other parties for computation purposes.

Here are our objectives for this module:

 * Understanding encryption and the issues related to it

 * Understanding what homomorphic encryption is and what can be done with it

 * Practical familiarity with Paillier encryption as one class of
   homomorphic encryption

 * Practical experience with speeding up homomorphic encryption with
   parallel computation using MPI (Message Passing Interface).

> ## Prerequisites
>
> * Basic operation of UNIX shell
> * Basic Python programming skill
> * Basic operation of SLURM job scheduler
{: .prereq}

{% comment %}

Pre-workshop survey link:

<https://odu.co1.qualtrics.com/jfe/form/SV_8uhUzd1TbeakZCt>
![PRE-survey-qr-wksp-5.png](fig/PRE-survey-qr-wksp-5.png)

{% endcomment %}

Website:
<https://deapsecure.gitlab.io/deapsecure-lesson05-crypt>

{% include links.md %}

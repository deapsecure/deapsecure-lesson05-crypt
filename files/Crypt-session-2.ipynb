{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**DeapSECURE module 5: Cryptography for Privacy-Preserving Computation (part A: Data Protection)**\n",
    "\n",
    "# Session 2: AES Encryption and Decryption\n",
    "\n",
    "Welcome to the DeapSECURE online training program!\n",
    "This is a Jupyter notebook for the hands-on learning activities of the\n",
    "[\"Cryptography\" module](https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/).\n",
    "*The contents of this notebook are new; they will be integrated into that lesson module at a later time.*\n",
    "Please visit the [DeapSECURE](https://deapsecure.gitlab.io/) website to learn more about our training program.\n",
    "\n",
    "In this session, we will learn the technical know-how to store & represent data on computers.\n",
    "\n",
    "<a id=\"TOC\"></a>\n",
    "**Quick Links** (sections of this notebook):\n",
    "\n",
    "* 1 [Setup](#sec-Setup)\n",
    "* 2 [A Simplistic Cipher - AES Library](#sec-AES_module)\n",
    "* 3 [Using Pycryptodome Library](#sec-Pycryptodome)\n",
    "* 4 [Encryption in Real World (Advanced)](#sec-Encryption_real)\n",
    "* 5 [Encryption Key Cracking](#sec-Cracking)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Setup\"></a>\n",
    "## 1. Setup Instructions\n",
    "\n",
    "If you are opening this notebook from Wahab cluster's OnDemand interface, you're all set.\n",
    "\n",
    "If you see this notebook elsewhere and want to perform the exercises on Wahab cluster, please follow the steps outlined in our setup procedure.\n",
    "\n",
    "1. Make sure you have activated your HPC service.\n",
    "2. Point your web browser to https://ondemand.wahab.hpc.odu.edu/ and sign in with your MIDAS ID and password.\n",
    "3. Create a new Jupyter session using \"legacy\" Python suite, then create a new \"Python3\" notebook. (See <a href=\"https://wiki.hpc.odu.edu/en/ood-jupyter\" target=\"_blank\">ODU HPC wiki</a> for more detailed help.)\n",
    "4. Get the necessary files using commands below within Jupyter:\n",
    "\n",
    "       mkdir -p ~/CItraining/module-crypt\n",
    "       cp -pr /shared/DeapSECURE/module-crypt/. ~/CItraining/module-crypt\n",
    "       cd ~/CItraining/module-crypt\n",
    "\n",
    "The file name of this notebook is `Crypt-session-2.ipynb`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Reminder\n",
    "\n",
    "* Throughout this notebook, `#TODO` is used as a placeholder where you need to fill in with something appropriate. \n",
    "* To run a code in a cell, press `Shift+Enter`.\n",
    "* Use `ls` to view the contents of a directory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 Loading Python Libraries\n",
    "\n",
    "<!--\n",
    "Now we need to **import** the required libraries into this Jupyter notebook:`numpy`.\n",
    "-->\n",
    "\n",
    "**Important**: On Wahab HPC, software packages, including Python libraries, are managed and deployed via *environment modules*.\n",
    "Before we can import the Python libraries in our current notebook, we have to load the corresponding environment modules.\n",
    "We have setup a custom environment \"DeapSECURE\" which will load all the required libraries for this workshop. Please load \"DeapSECURE\" module.\n",
    "\n",
    "* Load the modules above using the `module(\"load\", \"MODULE\")` or `module(\"load\", \"MODULE1\", \"MODULE2\", \"MODULE n\")` statement.\n",
    "* Next, invoke `module(\"list\")` to confirm that these modules are loaded.\n",
    "* In this module, we have setup a custom environment \"DeapSECURE\" including all the required libraries. Please load \"DeapSECURE\" module.\n",
    "\n",
    "<!-- (You can also setup your [custom environment](https://wiki.hpc.odu.edu/Software/Python#install-additional-python-modules)) -->"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Modify and uncomment statements below to load the required environment modules\"\"\";\n",
    "\n",
    "#module(\"load\", \"#TODO\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Confirm the loaded modules\"\"\";\n",
    "\n",
    "#TODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we go into encryption and decryption, we define the `encode_int`, `decode_int` and `leftpad16` helper functions as we did in session 1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def encode_int(C, minlength=16):\n",
    "    \"\"\"Encodes an arbitarily long integer into a `bytes` object.\n",
    "    The minimum length is by default 16 bytes (128 bits).\"\"\"\n",
    "    C_hex = hex(C)[2:]\n",
    "    if len(C_hex) % 2:\n",
    "        C_hex = '0' + C_hex\n",
    "    C_bytes = bytes.fromhex(C_hex)\n",
    "    if len(C_bytes) < minlength:\n",
    "        # pad the left side with NULLs\n",
    "        C_bytes = C_bytes.rjust(minlength, b'\\x00')\n",
    "    return C_bytes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def decode_int(B):\n",
    "    \"\"\"Decodes a `bytes` object into a long integer.\n",
    "    This is the converse of the `encode_int` function.\"\"\"\n",
    "    return int(B.hex(), 16)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def leftpad16(B):\n",
    "    \"\"\"Pad a bytes array from the left with NULL chars so that\n",
    "    the length is a multiple of 16 bytes.\"\"\"\n",
    "    padlength = len(B) % 16\n",
    "    if padlength > 0:\n",
    "        return (b'\\x00' * (16 - padlength)) + B\n",
    "    else:\n",
    "        return B"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-AES_module\"></a>\n",
    "## 2. A Simplistic Cipher: `aes.py`\n",
    "\n",
    "The `aes` module implements a very basic AES cipher that can only encrypt and decrypt exactly sixteen bytes (no more, no less).\n",
    "The module is written in pure Python.\n",
    "It is so short and simple to read for educational purposes.\n",
    "You are encouraged to read this module, located in your hands-on package:\n",
    "\n",
    "    ~/CItraining/AES/aes.py\n",
    "\n",
    "*(This module has been made available to you by loading the DeapSECURE module earlier.)*\n",
    "\n",
    "First, we load `aes` library:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's do some encryption and decryption using this module to illustrate the workings of AES.\n",
    "\n",
    "### 2.1 Define a Secret Key \n",
    "(*Ssssh, don't share this with anyone, okay?!*)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The master key (a secret) must be less than 128 bits (16 bytes):\n",
    "secret_key = 0x5e413c\n",
    "\n",
    "# Initializing \"E\", the object that can perform the encrypting / decrypting:\n",
    "E = aes.AES(secret_key)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Define a Plaintext to Encrypt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You can change any plaintext with 16 bytes in hexadecimal\n",
    "# This plaintext string must be under 16 letters:\n",
    "plaintext_string = 'IdeaFusion'\n",
    "plaintext = plaintext_string.encode() # utf-8 encoding\n",
    "\n",
    "# construct a long integer out of the bytes\n",
    "# because aes.py expects the data in that format for encryption and decryption:\n",
    "plaintext_int = decode_int(plaintext)\n",
    "\n",
    "print('The plaintext in bytes is:      ', plaintext)\n",
    "print('The plaintext in decimal is:    ', plaintext_int)\n",
    "print('The plaintext in hexadecimal is:', hex(plaintext_int))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**QUESTION**:\n",
    "\n",
    "* Does the conversion from `b'IdeaFusion'` to a hex string `49646561467573696f6e` count as encryption?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Encrypt the Plaintext\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The encrypt and decrypt functions in `aes.py` expect the data (plaintext and ciphertext) in the form of long (128-bit) integers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ciphertext_int = E.encrypt(plaintext_int)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "> **SIDEBAR**:\n",
    "In a Jupyter notebook, help can be obtained by using `FUNCTION_NAME?` syntax."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E.encrypt?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example above, the documentation did not indicate the datatype of the `plaintext` argument.\n",
    "However, the [test code](https://github.com/bozhu/AES-Python/blob/master/test.py) shows that the expected input and output are long integers.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**QUESTIONS:** How does the ciphertext look like?\n",
    "\n",
    "* How does it look in decimal (long integer) form?\n",
    "* How does it look in hexadecimal form?\n",
    "* Does it look like a readable string of bytes?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Now print the ciphertext and try to make sense of it:\"\"\";\n",
    "\n",
    "#TODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**EXERCISE**: Convert the ciphertext into `bytes` and see if it is meaningful (tips: use the `encode_int` function)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Convert the ciphertext into bytes and print it:\"\"\";\n",
    "\n",
    "#TODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**QUESTIONS**:\n",
    "\n",
    "* How long is the original message?\n",
    "* How long is the encrypted message?\n",
    "* Are they of the same length? Why, or why not?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.4 Decrypt the Ciphertext with the Correct Key\n",
    "\n",
    "Let's try to recover the original text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "decrypted_int = E.decrypt(ciphertext_int)\n",
    "print(\"Decrypted text in hexadecimal:\", hex(decrypted_int))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Decrypted text in bytes:\", encode_int(decrypted_int))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**EXERCISE**: How do we remove the trailing bytes?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.5 Decrypt the Ciphertext with a Wrong Key\n",
    "\n",
    "**EXERCISE**: Create another object named `E2` with a wrong key, e.g. `0x27F6A123`.\n",
    "Try recovering the message using that key--did it work?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Use this cell to work on the exercise above\"\"\";\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare the last \"decrypted\" plaintext against the original plaintext.\n",
    "Do they match?\n",
    "Try decrypting again with a few different keys and check the result.\n",
    "\n",
    "*HINT*: It is more convenient at this point if you create a Python function\n",
    "to perform the necessary sequence to decrypt and print the resulting message."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "> **THINK ABOUT IT!** What can we do to recover the original message if we lose the encryption key?\n",
    "(We will come back to this shortly.)\n",
    "\n",
    "---\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.6 Encrypt the Plaintext with a Different Key\n",
    "\n",
    "**EXERCISE**:\n",
    "Please try another experiment.\n",
    "Re-encrypt the plaintext message using a few very similar keys as the original one, and compare the ciphertext.\n",
    "Can you learn something from the new ciphertexts?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Original secret_key =\", hex(secret_key))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "key1 = secret_key + 1\n",
    "key2 = secret_key + 2\n",
    "key3 = secret_key + 3\n",
    "\n",
    "print(\"Try encrypting with at least the following keys:\")\n",
    "print(hex(key1))\n",
    "print(hex(key2))\n",
    "print(hex(key3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try encrypting with the keys printed above.\n",
    "Can you learn something from the new ciphertexts?\n",
    "Do they bear any similarity to the original ciphertext (`ciphertext_bytes` or `hex(ciphertext_int)`)?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.7 Performance Matters\n",
    "\n",
    "How fast is the `aes.py`-based encryption procedure?\n",
    "The following ipython magic runs the `E.encrypt` statement 1000 times in a loop, which is then repeated 10 times to get the statistics:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%timeit -n 1000 -r 10 E.encrypt(plaintext_int)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "During our development phase on Wahab cluster, it took ~0.2 seconds to run 1000 encryption process. How about decryption?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%timeit -n 1000 -r 10 E.decrypt(ciphertext_int)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It may be slightly longer, but not much longer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**QUESTIONS**:\n",
    "\n",
    "Given the timing above, estimate how long it wil take to crack:\n",
    "\n",
    "* an 8-bit key ($2^8$ = 256 combinations)?\n",
    "\n",
    "* a 16-bit key (how many combinations)?\n",
    "\n",
    "* a 32-bit key?\n",
    "\n",
    "* a 128-bit key?\n",
    "\n",
    "Assume we have to try all possible key combinations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Pycryptodome\"></a>\n",
    "## 3. Using PyCryptodome Library\n",
    "\n",
    "*(This exercise will be left for your own exploration)*\n",
    "\n",
    "Let's try using a different library to encrypt data. We use [PyCryptodome](https://pypi.org/project/pycryptodome/), which is a production-grade implementation of cryptographic algorithms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from Crypto.Cipher import AES"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*DO NOT be confused with the `aes.AES` earlier!*\n",
    "\n",
    "Here, `AES` actually refers to the `Crypto.Cipher.AES` submodule,\n",
    "whereas `aes.AES` refers to the simplistic class named `AES` inside the `aes` module.\n",
    "\n",
    "The calling convention for the `PyCryptodome` module is different from that of `aes`:\n",
    "`PyCryptodome` functions expects keys, input data and output data to be of `bytes` datatype instead of long integers.\n",
    "Once you understand this, using `PyCryptodome` is quite easy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate the same secret key in the form of 16-byte:\n",
    "secret_bkey = encode_int(secret_key)\n",
    "print(\"secret_key in hexadecimal (128-bit):\", secret_bkey.hex())\n",
    "print(\"secret_key in bytes: \", secret_bkey)\n",
    "\n",
    "# Create a new encryptor class\n",
    "EE = AES.new(secret_bkey, AES.MODE_ECB)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> !!!**IMPORTANT SECURITY NOTE**!!!\n",
    ">\n",
    "> The AES object above was created with the *ECB* mode.\n",
    "> This is ok only for playing with encryption here, but you do NOT want to use ECB for your real encryption work, as it is easy for snoopers to understand what you are doing, and it is relatively easy to crack ECB-encrypted messages.\n",
    "> A [graphical illustration on Wikipedia](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Electronic_codebook_(ECB)) shows the point.\n",
    "> With ECB, the encrypted \"Tux\" penguin picture can still be visually deduced, therefore not all information is hidden yet:\n",
    ">\n",
    "> ![Penguin picture encrypted with ECB](fig/Tux_ecb.jpg)\n",
    ">\n",
    "> (Credit: Wikipedia user \"Lunkwill\".\n",
    "> Derived from the Tux picture by Larry Ewing, <lewing@isc.tamu.edu>, and The GIMP project.)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1 Encryption"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ciphertext_bytes3 = EE.encrypt(leftpad16(b'IdeaFusion'))\n",
    "ciphertext_bytes3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare this with the `ciphertext_bytes` earlier:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Print ciphertext_bytes and ciphertext_bytes3\"\"\";\n",
    "\n",
    "#TODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2 Decryption"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "decrypted_bytes3 = EE.decrypt(ciphertext_bytes3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(decrypted_bytes3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.3 Performance of PyCryptodome\n",
    "\n",
    "Now, how fast is `PyCrypyodome`-based crypto procedure?\n",
    "\n",
    "Encyrption:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %timeit #TODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Decryption:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %timeit #TOODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**QUESTION**: \n",
    "\n",
    "* The PyCryptodome performance is at least 30x that of `aes.py`? Why?\n",
    "\n",
    "To understand this, you must look at the source code.\n",
    "AES encryption and decryption operations are compute-intensive, and they work on bit-by-bit level.\n",
    "The `aes.py` is written in pure Python, therefore the performance is very low.\n",
    "PyCryptodome, on the other hand, has the performance-sensitive encryption and decryption procedures written in C, therefore they can achieve near-peak performance of the machine.\n",
    "\n",
    "On the other hand, `aes.py` is small and is guaranteed to work as long as you abide in its restrictions.\n",
    "In a learning environment like this one, or in an extremely constrained environment where performance is not critical, a simplistic implementation can be helpful."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The PyCryptodome can encrypt longer strings with ease, as long as the message is padded correctly to the multiple of 16-bytes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plain_bytes4 = leftpad16(b'The master key (a secret) must be less than 128 bits (16 bytes)')\n",
    "print(len(plain_bytes4))\n",
    "(plain_bytes4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cipher_bytes4 = EE.encrypt(plain_bytes4)\n",
    "cipher_bytes4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "EE.decrypt(cipher_bytes4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Encryption_real\"></a>\n",
    "## 4. Encryption in Real World (Advanced)\n",
    "\n",
    "In real world, data encryption is a serious business.\n",
    "Some strong advise:\n",
    "\n",
    "1. Stay with well-established algorithms and procedures.\n",
    "   They have proven track record of security.\n",
    "\n",
    "2. Do not try to roll your own encryption procedure unless you know exactly what you are doing.\n",
    "   For sure, this is not something that a novice should do,\n",
    "   because we can easily weaken encryption to the point that it can easily be broken.\n",
    "   Remember that hackers have long arms and can figure things out even when we think we are secure enough.\n",
    "\n",
    "3. Make sure that you are using up-to-date crypto tools and algorithms.\n",
    "   Avoid using an algorithm or tool that is already known to have weaknesses.\n",
    "   For example: Avoid using obsolete encryption scheme (such as 3DES), because they can easily be cracked.\n",
    "   Another example: `PyCryptodome` is an up-to-date fork of the original crypto library called `PyCrypto`.\n",
    "   The former is still being actively updated, whereas its parent has not been updated for six years (as of year 2020).\n",
    "\n",
    "In Python, there is another highly respected crypto library named [cryptography](https://pypi.org/project/cryptography/).\n",
    "Do look into that project as well to meet your need of data protection."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4.1 AES in Real World"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Earlier we said that ECB is not the correct way to use AES in real-world applications, because it makes it easy for attackers to at least \"see\" the long-range structure of the encrypted data.\n",
    "ECB is the \"plain vanilla\" AES without additional measures to safeguard the data against tampering or learning.\n",
    "The advanced block cipher modes such as EAX, GCM, CCM, OCB augment the \"vanilla\" AES with authentication and block-chaining techniques.\n",
    "We will demonstrate *EAX* below; for the rest, please read the recommended readings below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "EAX = AES.new(secret_bkey, AES.MODE_EAX)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "EAX_nonce = EAX.nonce;\n",
    "print(\"EAX.nonce = \", EAX_nonce)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note: *Nonce* is a random number that is used only once in the encryption process.\n",
    "A new EAX context will use a different nonce."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "crypt_EAX = EAX.encrypt(leftpad16(b'The master key (a secret) must be less than 128 bits (16 bytes)'))\n",
    "crypt_EAX"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Decryptor class\n",
    "# (will need a separate class because the EAX method is not stateless)\n",
    "DD_EAX = AES.new(secret_bkey, AES.MODE_EAX, nonce = EAX_nonce)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "DD_EAX.decrypt(crypt_EAX)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**EXERCISE**:\n",
    "Repeat the encrytion and decryption procedures above severial times.\n",
    "Do you notice any difference compared to the previous ones?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "EE_EAX2 = AES.new(secret_bkey, AES.MODE_EAX)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"EE_EAX2.nonce = \", EE_EAX2.nonce)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "crypt_EAX2 = EE_EAX2.encrypt(leftpad16(b'The master key (a secret) must be less than 128 bits (16 bytes)'))\n",
    "crypt_EAX2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "DD_EAX2 = AES.new(secret_bkey, AES.MODE_EAX, nonce=EE_EAX2.nonce)\n",
    "DD_EAX2.decrypt(crypt_EAX2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**IMPORTANT:** The EAX encryption always differs every time because it draws different `nonce`:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4.2 Recommended Readings\n",
    "\n",
    "These are good references for people who want to practice encryption in real world.\n",
    "\n",
    "* https://pycryptodome.readthedocs.io/en/latest/src/examples.html#encrypt-data-with-aes \\\n",
    "An example good practice to encrypt and decrypt data with AES.\n",
    "\n",
    "* https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation \\\n",
    "Some explanation of different block cipher modes, including schematic explanation of these modes.\n",
    "\n",
    "* https://blog.cryptographyengineering.com/2012/05/19/how-to-choose-authenticated-encryption/ \\\n",
    "Reasons to use authentication authentication encryption."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Cracking\"></a>\n",
    "## 5. 3ncrypt1on K3y Cr4cking !!\n",
    "\n",
    "Now comes the exciting part!\n",
    "Let's play hacker and crack a given ciphertext.\n",
    "How do we crack an encrypted message if we don't know anything about the key?\n",
    "The only sure way to recover the message is to try all the possible values of the key, decrypt the message, then figure if the decrypted message makes sense.\n",
    "\n",
    "For a fully unknown 128-bit key, we wil have to try $2^{128}$ combinations (over $3 \\times 10^{38}$).\n",
    "That is truly daunting!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# total combination of 128-bit key\n",
    "\n",
    "\"%g\" % (2**128)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How Difficult Is This Problem?\n",
    "\n",
    "**QUESTION:**\n",
    "Suppose a high-speed cracker can try thru all combinations of a 24-bit key in 5 minutes.\n",
    "Then how many hours are required to crack an AES-128 key with N unknown bits?\n",
    "\n",
    "* 32-bits = 256 &times; 5 minutes = 21 hours\n",
    "* 48-bits = 65536 & times; 21 hours = 1.38 million hours = over 57,000 days!!\n",
    "\n",
    "*Always do a back-of-envelope calculation like this before you launch something computationally expensive!*\n",
    "You get the point... a full 128-bit key cracking is clearly out of reach even for today's mega-supercomputers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### OUR CRACKING CHALLENGE\n",
    "\n",
    "Here is the formal definition of our AES-128 cracking challenge.\n",
    "\n",
    "#### Reduced Key Space\n",
    "\n",
    "For the reason elaborated above, we significantly reduce the number of unknown bits in the AES-128 key space.\n",
    "We prepare some challenges with increasing levels of difficulty:\n",
    "\n",
    "| key bits  |  Starting key (hex)                | Ending key (hex)                   | Total combinations     |\n",
    "|-----------|------------------------------------|------------------------------------|------------------------|\n",
    "|       8   | `00000000000000000000000000000000` | `000000000000000000000000000000ff` | 256                    |\n",
    "|      16   | `00000000000000000000000000000000` | `0000000000000000000000000000ffff` | 65,536                 |\n",
    "|      20   | `00000000000000000000000000000000` | `000000000000000000000000000fffff` | 1,048,576              |\n",
    "|      24   | `00000000000000000000000000000000` | `00000000000000000000000000ffffff` | 16,777,216             |\n",
    "|      32   | `00000000000000000000000000000000` | `000000000000000000000000ffffffff` | 4,294,967,296          |\n",
    "\n",
    "The \"starting key\" and \"ending key\" above defines the smallest and largest numerical values to try for the AES-128 key.\n",
    "By 32 bits, there are already nearly 4.3 billion combinations to try, so it requires HPC to solve it as soon as possible.\n",
    "\n",
    "#### Plaintext Message\n",
    "\n",
    "The secret plaintext messages are known to contain *only* letters (A-Z, a-z).\n",
    "No whitespaces, no numbers, no symbols."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Verifying Your Cracker\n",
    "\n",
    "Here are some ciphertexts with cracked messages to verify that your _cr4cker w0rks (!!)_\n",
    "\n",
    "| key bits | ciphertext (hex)                   | plaintext                    |\n",
    "|----------|------------------------------------|------------------------------|\n",
    "|        8 | `f163060b1e6c68753c637854b838609e` | `IHaveNoIdea`                |\n",
    "|        8 | `d3bbd04550497f943f6bf4c9e2291993` | `CongratsYouDidIt`           |\n",
    "|       16 | `c7c88a08cd82ef5e8afb051e9cebe122` | `AESBetterThanDES`           |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

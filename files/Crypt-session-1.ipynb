{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**DeapSECURE module 5: Cryptography for Privacy-Preserving Computation (part A: Data Protection)**\n",
    "\n",
    "# Session 1: Encoding: Representing Data on Computers\n",
    "\n",
    "Welcome to the DeapSECURE online training program!\n",
    "This is a Jupyter notebook for the hands-on learning activities of the\n",
    "[\"Cryptography\" module](https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/),\n",
    "Episode 3: [\"Encoding and Encrypting Data Using Python Libraries\"](https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/03-python-intro/index.html).\n",
    "Please visit the [DeapSECURE](https://deapsecure.gitlab.io/) website to learn more about our training program.\n",
    "\n",
    "In this session, we will learn the technical know-how to store & represent data on computers.\n",
    "\n",
    "<a id=\"TOC\"></a>\n",
    "**Quick Links** (sections of this notebook):\n",
    "\n",
    "* 1 [Setup](#sec-Setup)\n",
    "* 2 [Integers, Bits, and Hexadecimal Numbers](#sec-Integers-Bits-Hex)\n",
    "* 3 [Strings and Bytes](#sec-Strings-Bytes)\n",
    "* 4 [Hexadecimal Representation](#sec-Hex-Representation)\n",
    "* 5 [Encoding and Decoding Integers](#sec-Encode-Decode-Ints)\n",
    "* 6 [Padding and Unpadding](#sec-Pad-Unpad)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Setup\"></a>\n",
    "## 1. Setup Instructions\n",
    "\n",
    "If you are opening this notebook from Wahab cluster's OnDemand interface, you're all set.\n",
    "\n",
    "If you see this notebook elsewhere and want to perform the exercises on Wahab cluster, please follow the steps outlined in our setup procedure.\n",
    "\n",
    "1. Make sure you have activated your HPC service.\n",
    "2. Point your web browser to https://ondemand.wahab.hpc.odu.edu/ and sign in with your MIDAS ID and password.\n",
    "3. Create a new Jupyter session using \"legacy\" Python suite, then create a new \"Python3\" notebook. (See <a href=\"https://wiki.hpc.odu.edu/en/ood-jupyter\" target=\"_blank\">ODU HPC wiki</a> for more detailed help.)\n",
    "4. Get the necessary files using commands below within Jupyter:\n",
    "\n",
    "       mkdir -p ~/CItraining/module-crypt\n",
    "       cp -pr /shared/DeapSECURE/module-crypt/. ~/CItraining/module-crypt\n",
    "       cd ~/CItraining/module-crypt\n",
    "\n",
    "The file name of this notebook is `Crypt-session-1.ipynb`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Reminder\n",
    "\n",
    "* Throughout this notebook, `#TODO` is used as a placeholder where you need to fill in with something appropriate. \n",
    "* To run a code in a cell, press `Shift+Enter`.\n",
    "* Use `ls` to view the contents of a directory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 Loading Python Libraries\n",
    "\n",
    "<!--\n",
    "Now we need to **import** the required libraries into this Jupyter notebook:`numpy`.\n",
    "-->\n",
    "\n",
    "**Important**: On Wahab HPC, software packages, including Python libraries, are managed and deployed via *environment modules*.\n",
    "Before we can import the Python libraries in our current notebook, we have to load the corresponding environment modules.\n",
    "We have setup a custom environment \"DeapSECURE\" which will load all the required libraries for this workshop. Please load \"DeapSECURE\" module.\n",
    "\n",
    "* Load the modules above using the `module(\"load\", \"MODULE\")` or `module(\"load\", \"MODULE1\", \"MODULE2\", \"MODULE n\")` statement.\n",
    "* Next, invoke `module(\"list\")` to confirm that these modules are loaded.\n",
    "* In this module, we have setup a custom environment \"DeapSECURE\" including all the required libraries. Please load \"DeapSECURE\" module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Modify and uncomment statements below to load the required environment modules\"\"\";\n",
    "\n",
    "#module(\"load\", \"#TODO\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Confirm the loaded modules\"\"\";\n",
    "\n",
    "#TODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Integers-Bits-Hex\"></a>\n",
    "## 2. Integers, Bits, and Hexadecimal Numbers\n",
    "\n",
    "Encryption and decryption is a low-level operation: It operates on data at its lowest level of representation: *binary bits*.\n",
    "In modern computers, bits are bundled into *bytes*.\n",
    "(An example: the size of a computer drive is often measured in the units of gigabytes = 1 billion bytes.)\n",
    "\n",
    "A *byte* consists of eight bits (each bit can be 0 or 1);\n",
    "these eight bits make up a short integer:\n",
    "\n",
    "    8-bits        hexadecimal   decimal value\n",
    "    00000000   =>     0x00    =>     0\n",
    "    00000001   =>     0x01    =>     1\n",
    "    00000010   =>     0x02    =>     2\n",
    "    00000011   =>     0x03    =>     3\n",
    "    ...\n",
    "    11111110   =>     0xFE    =>   254\n",
    "    11111111   =>     0xFF    =>   255\n",
    "\n",
    "A byte, therefore, is equivalent to a short integer, whose value can be 0, 1, 2, ... through 255.\n",
    "Integers on computer are often expressed in the *hexadecimal* (base-16) representation instead of bits (binary, or base-2 representation) due to its conciseness.\n",
    "A hexadecimal number system uses 16 possible values (0 through 9, followed by A through F):\n",
    "\n",
    "    decimal values:\n",
    "    0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15\n",
    "    hexadecimal:\n",
    "    0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F\n",
    "\n",
    "Any byte can be represented using just two hexadecimal digits.\n",
    "A hexadecimal number is often written with either the `0x` prefix or the `h` suffix to prevent confusion with the customary decimal numbers (base-10).\n",
    "\n",
    "In a nutshell, *at the lowest level, all types of data on computers are represented as integers* (i.e. as bits and bytes), regardless whether they are interpreted by us as numbers, texts, images, sounds, videos, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Strings-Bytes\"></a>\n",
    "## 3. Strings and Bytes\n",
    "\n",
    "Let us learn some things about strings and bytes, as they will help us working correctly later in the encryption and decryption process.\n",
    "\n",
    "In Python, a *string* defines a sequence of characters as understood by human. Recent internationalization effort resulted in a universal coding called *Unicode*, whereby Python `str` can contain characters from arbitrary scripts (not just Latin, but Arabic, Japanese, Chinese, etc.):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Create a \"Hello world\" string and save that as a variable named `S`:\"\"\";\n",
    "\n",
    "#S = \"#TODO\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Print the value of S\"\"\";\n",
    "\n",
    "#print(#TODO)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try to include other characters in the string, for example Arabic, Japanese, Chinese, French etc. (This may be tricky to do if your OS supports only US English language. You can browse characters on this website: http://www.unicode.org/charts/ . Alternatively, you can open your favorite word processor and use the \"Insert\" -> \"Special Symbol\" functionality.)\n",
    "\n",
    "Here's an example with a greeting in Chinese:\n",
    "\n",
    ">  早上好, friend!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Create a string contains non-latin characters, then print it:\"\"\";\n",
    "\n",
    "#S_ZH = #TODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python defines a special datatype called `bytes`, which is simply a string of bytes.\n",
    "Any variable and value in Python (whether an integer or a string) can be converted into its `bytes` equivalent.\n",
    "\n",
    "Encryption works on the byte-representation of the data.\n",
    "Therefore, before we try to encrypt and decrypt data, it is very important that we understand how to:\n",
    "\n",
    "* *encode* the high-level data into the low-level `bytes`, and\n",
    "* *decode* (reconstruct) `bytes` back into the high-level data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let us encode the characters in `S` to get their low-level byte representation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Encode the string `S` to bytes:\"\"\";\n",
    "\n",
    "B = bytes(S, encoding=\"utf-8\")\n",
    "print(B)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This is a different way to accomplish the same result:\n",
    "# the \"utf-8\" argument can even be omitted; it is already the default\n",
    "\n",
    "B_alt = S.encode(\"utf-8\")\n",
    "print(B_alt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In contrast to strings, Python will enclose the contents of a `bytes` variable with `b'...'`.\n",
    "\n",
    "How long is the string `S` (how many characters)?\n",
    "How long is `B` (how many bytes)?\n",
    "(*Hint*: In both cases, use the `len()` function to find out.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Print the length of `S` and `B`\"\"\";\n",
    "\n",
    "#print(len(#TODO))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now observe what happens when we encode the other string (`S_ZH`) that has non-latin characters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Try to encode B_ZH string to bytes as well.\"\"\";\n",
    "\n",
    "#B_ZH = #TODO "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Print the lengths of `S_ZH` and `B_ZH`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#TODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**QUESTIONS:**\n",
    "\n",
    "* Why does `B_ZH` look different from `B`?\n",
    "* Why do the lengths of `S_ZH` and `B_ZH` differ? Which one is longer?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The array of bytes can be decoded back to a string (using UTF-8 encoding, by default):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Decode bytes into string\"\"\";\n",
    "\n",
    "B_ZH.decode()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Historically, a character was represented as a byte (each byte value corresponding to a particular character). **ASCII** was the most widely accepted standard of character mapping.\n",
    "Only values 0..127 have a defined meaning in ASCII, as you can see from [the ASCII character table](https://commons.wikimedia.org/wiki/File:ASCII_Code_Chart.svg) below.\n",
    "For example, the letters `A` and `a` have the ASCII codes of `0x41` and `0x61`, respectively (65 and 97 in decimal).\n",
    "\n",
    "![ASCII character table](fig/500px-ASCII_Code_Chart.png)\n",
    "\n",
    "(from: https://commons.wikimedia.org/wiki/File:ASCII_Code_Chart.svg)\n",
    "\n",
    "In Python, bytes that have the printable ASCII character representation will be printed using the corresponding characters.\n",
    "The other values have no printable representation, therefore they appear as hexadecimal numbers.\n",
    "For example, in\n",
    "\n",
    "    b'\\xe6\\x97\\xa9\\xe4\\xb8\\x8a\\xe5\\xa5\\xbd, friend!'\n",
    "\n",
    "the substring `\\xe6` stands for a byte value `0xE6` = 230 in decimal."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Today, [**UTF-8**](https://en.wikipedia.org/wiki/UTF-8) is widely used in computing systems, extending and superseding ASCII by accommodating more than just 256 possible characters. UTF-8 is backward compatible with ASCII.\n",
    "\n",
    "For example, in UTF-8, each Chinese letter has a three-byte representation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(str(b'\\xe6\\x97\\xa9', 'utf-8'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some other characters such as accented Latin letters (`Ě`, `à`, `é`, and so on) occupy two bytes per character."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"print other characters\"\"\";\n",
    "\n",
    "#print(bytes(\"#TODO\", encoding=\"utf-8\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Lesson**: Strings that contain non-Latin letters have longer byte representations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Hex-Representation\"></a>\n",
    "## 4. Hexadecimal Representation\n",
    "\n",
    "Strictly speaking, a hexadecimal representation of binary data is not a separate encoding from bytes.\n",
    "Instead of printing some bytes as literal characters and the rest in hexadecimal numbers, everything is simply printed as hexadecimal digits.\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "B.hex()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The hex string line `48656c6c6f20776f726c64` means a sequence of bytes with hex values: `48`, `65`, `6c`, `6c`, `6f`, and so on. Here, `48` hex = 16&times;4 + 8 = 72 decimal, the character code for letter `H`. Similarly, the subsequent hex values map to the subsequent characters (`e`, `l`, `l`, and so on...). You can check the [ASCII table](https://www.ascii-code.com/).\n",
    "\n",
    "A hex string representation is exactly twice as long as the byte representation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Check the length of hex string\"\"\";\n",
    "\n",
    "#len(#TODO)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Print the length of the hex string of `B_ZH` defined earlier.\"\"\";\n",
    "#TODO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Conversely, a `bytes` object can be created from a hex string or from an array of integers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S2 = bytes.fromhex(\"476f6f64206d6f726e696e67\")\n",
    "print(S2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S3 = bytes([0x47, 0x6f, 0x6f, 0x64, 0x20,\n",
    "            0x6d, 0x6f, 0x72, 0x6e, 0x69, 0x6e, 0x67])\n",
    "print(S3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Encode-Decode-Ints\"></a>\n",
    "## 4. Encoding and Decoding Integers\n",
    "\n",
    "Now let us turn to the integer data type.\n",
    "Let us define two routines to help us with the mundane tasks of encoding and decoding integers:\n",
    "\n",
    "**Function: `encode_int`**\n",
    "\n",
    "This function converts an integer of arbitrary magnitude to a `bytes` object.\n",
    "In the crypto convention, the most significant digit will be the first byte in the array.\n",
    "If the conversion results in fewer than `minlength` bytes, it will be padded from the left with zeros (i.e., ASCII NULL characters).\n",
    "By default, we will create a 16-byte long string because the (1) AES routines process 16-byte blocks of data and (2) we will be using AES with 128-bit keys."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def encode_int(C, minlength=16):\n",
    "    \"\"\"Encodes an arbitarily long integer into a `bytes` object.\n",
    "    The minimum length is by default 16 bytes (128 bits).\"\"\"\n",
    "    # The hex function will add `0x` prefix, they must be stripped off\n",
    "    C_hex = hex(C)[2:]\n",
    "    if len(C_hex) % 2:\n",
    "        C_hex = '0' + C_hex\n",
    "    C_bytes = bytes.fromhex(C_hex)\n",
    "    if len(C_bytes) < minlength:\n",
    "        # pad the left side with NULLs\n",
    "        C_bytes = C_bytes.rjust(minlength, b'\\x00')\n",
    "    return C_bytes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example to encode an integer to bytes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = 0x2A3749\n",
    "print(\"A in decimal =\", A)  # in decimal\n",
    "A_bytes = encode_int(A)\n",
    "A_bytes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A_bytes.hex()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> An integer object has a `to_bytes` method that does almost the same thing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A.to_bytes(16, 'big')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The `big` argument refers to the exact data layout known as [*endianness*](https://en.wikipedia.org/wiki/Endianness). We do not need to delve into that issue here, except that we are working with the *big endian* layout.\n",
    "The `encode_int` function is useful if\n",
    "(1) you may have an arbitrarily long integer to encode; and/or\n",
    "(2) you do not want the NULL padding (set `minlength=0`)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Function: `decode_int`**\n",
    "\n",
    "This function converts a `bytes` object into a long integer. This is the converse of the `encode_int` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def decode_int(B):\n",
    "    \"\"\"Decodes a `bytes` object into a long integer.\n",
    "    This is the converse of the `encode_int` function.\"\"\"\n",
    "    return int(B.hex(), 16)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Here is an example to reconstitute `A`\"\"\"\n",
    "\n",
    "decode_int(A_bytes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Verify if the operation results in the same value:\"\"\"\n",
    "\n",
    "decode_int(A_bytes) == A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we successfully encode a text string or an integer in terms of bytes, then we can proceed with encryption."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Strings as Long Integers?\n",
    "\n",
    "Earlier we have hex representation of a byte strings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "B"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "B.hex()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows that we can also represent a byte string as a very long integer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Converts the hex representation of B to an integer\"\"\"\n",
    "# 16 is the base of the number (base-16)\n",
    "\n",
    "B_int = int(B.hex(), 16)\n",
    "B_int"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**CONCLUSION**: the four objects below represent the same data:\n",
    "\n",
    "* a Unicode string: `'Hello world'`\n",
    "* a byte string: `b'Hello world'`\n",
    "* a hex string: `48656c6c6f20776f726c64`  (optionally prefixed with `0x`)\n",
    "* a long integer: `87521618088882671231069284`\n",
    "\n",
    "Now that we know how to convert between the different representations, we can convert a string message to a form suitable for encryption."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"sec-Pad-Unpad\"></a>\n",
    "## 6. Padding and Unpadding\n",
    "\n",
    "AES is a *block cipher*:\n",
    "An AES cipher operates on a 16-byte block of data at once,\n",
    "and it expects an input that is a multiple of 16 bytes.\n",
    "If the input size does not conform to this requirement, we must pad the input data so that its size is a multiple of 16 bytes.\n",
    "\n",
    "*Padding* address the concerns such as:\n",
    "\"What if my message is shorter than 16 characters?\"\n",
    "\"What if my message is 30 bytes long?\"\n",
    "\n",
    "In this hands-on example, we simply pad the non-conforming block with enough zeros (NULLs) from the left, so that the length of the `bytes` become 16, 32, 48, and so on.\n",
    "To unpad a data block, we will simply strip all the leading NULLs.\n",
    "(We can do this, because our valid messages will not contain any NULL character.\n",
    "For paddings used in real-world applications, please see the next notebook.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Function: `leftpad16`**\n",
    "\n",
    "This function pads the values in the `bytes` object with just enough zeros from the left so that the length of the resulting array is a multiple of 16.\n",
    "We need this padding because AES is a *block cipher*,\n",
    "which means it operates on a 16-byte block of data at once.\n",
    "AES cipher expects an input that is a multiple of 16 bytes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def leftpad16(B):\n",
    "    \"\"\"Pad a bytes array from the left with NULL chars so that\n",
    "    the length is a multiple of 16 bytes.\"\"\"\n",
    "    padlength = len(B) % 16\n",
    "    if padlength > 0:\n",
    "        return (b'\\x00' * (16 - padlength)) + B\n",
    "    else:\n",
    "        return B"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Here is an example to do leftpadding\"\"\";\n",
    "\n",
    "msg = b'ODU is great'\n",
    "msg_pad = leftpad16(msg)\n",
    "msg_pad"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Try to padding msg2\"\"\"\n",
    "\n",
    "msg2 = b'We need padding because AES is a block cipher'\n",
    "#msg2_pad = #TODO"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Print the length of bytes\"\"\"\n",
    "\n",
    "# print(\"Length of original msg:\", #TODO)\n",
    "# print(\"Length of original msg with padding:\", #TODO)\n",
    "# print(\"Length of original msg2:\", #TODO)\n",
    "# print(\"Length of original msg2 with padding:\", #TODO)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"Removal of the leading NULLs can be done using the lstrip method:\"\"\"\n",
    "\n",
    "msg_pad.lstrip(b'\\x00')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "msg2_pad.lstrip(b'\\x00')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### You Did It!\n",
    "\n",
    "This is the end of the first notebook!\n",
    "If you have reach this point, you can continue the next notebook by opening `Crypt-session-2.ipynb` in your Jupyter session."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

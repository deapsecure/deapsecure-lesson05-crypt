---
title: "Outro: Applications of Homomorphic Encryption"
teaching: 0
exercises: 0
questions:
- "What are the important applications of homomorphic encryption?"
objectives:
- "where the use of homomorphic encryption is appropriate?"
keypoints:
- "Homomorphic encryption can be used to perform machine learning or compute aggregates without revealing private data."
---




## Homomorphic Encryption for Privacy-Preserving Machine Learning

Big data (BD) and machine learning (ML) have revolutionized
society today.
Cloud computing has pushed the boundary of what can be done with
BD and ML.
There are many valid reasons we want to do computing in the cloud.
For example, the data may originate from scattered sources (e.g.
customer's smart devices); they need to be collected or aggregated
in a central location.
Another case may be because the "near" computing devices may not have
all the capabilities to handle the required computation or data.
With so many resources available in the cloud,
it is becoming attractive to offload computation to the cloud.


### Security & Trust Issues in Cloud Computing

As machine learning (ML) are becoming more pervasive, the world become
more connected by internet-of-things and mobile computing, and much of
the data are stored and processed in the cloud, issues concerning data
privacy and security has become more pressing.
Beyond the theft of the valuable data, adversaries could potentially
disrupt the reliability of ML by poisoning the training data.
Additionally, ML can also be leveraged as a double-edged sword to
break into user privacy, e.g. to "predict" the indisclosed data from  "reverse-engineer" the data from.
Cybersecurity researchers are actively developing countermeasures to
guard against these attacks.

Cloud computing also poses a trust issue, which is deeper than the
cybersecurity issues above.
In many cases, the cloud may be a service provider (think of Google
that aggregates data from our smart phones for traffic status) which
is a separate entity from the data owner.
In such cases, users may not trust the service provider that they can
safeguard their data against theft and misuse.
The service provider, on the other hand, may not trust their software
to be running on their user's end in fear of leaking intellectual
property or unauthorized modifications.


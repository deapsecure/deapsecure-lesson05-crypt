---
title: "Supplementary Info: Homomorphic Encryption Implementations"
teaching: 0
exercises: 0
questions:
- "What are some implementations of homomorphic encryption?"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "Some implementations of FHE include Microsoft SEAL, PALISADE, and IBM"
---


## Free/Open-Source Homomorphic Encryption Libraries

The following is an incomplete list of software libraries (toolboxes)
that can be used to perform computation with HE.

* [Microsoft SEAL](
      https://www.microsoft.com/en-us/research/project/microsoft-seal/
  )
  (alternate link: <http://sealcrypto.org>) ---
  an implementation of fully homomorphic encryption.
  The implementation is in C++; there is also an interface to C#.

  * Github resources: <https://github.com/Microsoft/SEAL>

* [PALISADE](
      https://git.njit.edu/palisade/PALISADE/wikis/home      
  ) ---
  annother implementation of fully homomorphic encryption.
  The implementation is written in C++11.

  * Github resources: <https://git.njit.edu/palisade/PALISADE>

* [HElib](
      https://shaih.github.io/HElib/
  ) ---
  IBM's implementation of fully homomorphic encryption.
  The implementation is written in C.

  * Github resources: <https://github.com/shaih/HElib>

  * A blog article: ["2 + 3" Using HElib](
        https://mshcruz.wordpress.com/2016/06/17/2-3-using-helib/
    )

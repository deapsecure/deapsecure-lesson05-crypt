---
title: "Breaking Modified AES-256 Encryption"
teaching: 0
exercises: 0
questions:
- "Hands-on 1: Cracking Modified AES-256 Encryption"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "First key point. Brief Answer to questions. (FIXME)"
---

## About Encryption Cracking

We will begin our hands-on session by a common cybersecurity problem:
cracking encryption.

Just as locksmiths are needed from time to time to open doors in the
case of lockouts, there are legitimate uses for password and
encryption cracking.
You may have heard of governments breaking into encryptions in order
to thwart enemy's attacks.
Another instance is the case of a legitimate data or resource owner
forgets his/her own password or key to unlock the resources.
Here is an example from Jeremiah Grossman when he had to request
help from some trusted colleagues to
[recover his encrypted disk because he forgot his password](
    https://www.whitehatsec.com/blog/cracking-aes-256-dmgs-and-epic-self-pwnage/
).
Jeremiah's colleague used a computer cluster with GPUs to make this
possible.

In this session, we will be following a procedure by Github user
`snovvcrash` to break a modified AES-256 encryption.
The software is published here:
<https://github.com/snovvcrash/aes256m-cracker>.

**AES** refers to the
[Advanced Encryption Standard](
    https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
) developed in late 1990s and was adopted by
U.S. National Institute of Standards and Technology (NIST) as
the [encryption standard](
    https://nvlpubs.nist.gov/nistpubs/fips/nist.fips.197.pdf
) in 2001.
It is widely used worldwide today.
AES uses a symmetric-key encryption algorithm, meaning that the
same key is used for encryption and decryption.
This key therefore must be kept as a secret.
The length of the key can be 128, 192, 256 bits long; the longer
the key, the stronger the encryption.

In a nutshell, AES uses a sequence of reversible bit scrambling
operations involving the secret key and a carefully crafted
byte-for-byte mapping called "S-box" (short for *subtitution box*).

The hands-on we are doing here demonstrates the importance of
carefully selecting parameters in cryptography.

The original AES algorithm 



U.S. Government
(NIST) 

We 

1. Crack an encryption system (simplified AES): Doing test on HPC.

Step1: log in to the HPC platform with your ID and password;

Step2: load some necessary package as: 

```bash
enable_lmod
module load slurm
module load python/3.7
module load gcc/6
module load numpy 
```

and the current loaded module is as follows:

Then we will go to the aes256m-cracker file (at the location along with this manual)
(modified from https://github.com/snovvcrash/aes256m-cracker)

Step3: go to the aes256m_cpp file and run “make”;
It will show you the process of building project as 

This project will encrypt a plaintext.txt file by AES, and output another encrypted file called “ciphertext”.

Step4: encrypt the plaintext into ciphertext by running
./aes256m_cpp -e -m ECB -i plaintext.txt -o ciphertext -p v3ry_s3cr3t_p4ssw0rd && make clean
This will output a ciphertext and show you the size of the file and the time as follows.

Then abstract the first block of plaintext by typing
xxd plaintext.txt | head -n 1 | cut -d " " -f 2-9 | tr -d " "
we will get efbbbf5468652050726f6a6563742047
Step5: Go to cd `../cracker` and run the following .sh file to crack the encrypted ciphertext.

```bash
#!/bin/bash -l
#SBATCH --output=aes_cracker_output
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
python3 cracker.py efbbbf5468652050726f6a6563742047 ../aes256m_cpp/ciphertext
```
By using many cores for decryption, we have approximate decryption time as follows.
1 core
10 cores
20 cores
886 s
797 s
442 s
(another ciphertext)
In the ../aes256m_cpp file, there is another plaintext called “plaintext1.txt”, you are supposed to repeat step4 and step5 to achieve the crack.



{% include links.md %}


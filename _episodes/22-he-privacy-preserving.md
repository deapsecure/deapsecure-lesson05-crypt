---
title: "Privacy-Preserving Computation with Homomorphic Encryption"
teaching: 0
exercises: 0
questions:
- "Hands-on 2: How to use homomorphic encryption for privacy preserving computation?"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "First key point. Brief Answer to questions. (FIXME)"
---

## Privacy-Preserving, Multiparty Computation

In the introduction we have mentioned that HE are important for
protecting people's private data while allowing statistical aggregation
and machine learning on them.
Statistical aggregation such as average value calculation is
straightforward to do using HE.
In this episode, we will focus on doing simple computation and aggregation 
using Paillier HE.
The use of HE for machine learning (especially for neural network)
is still a subject of intense research.
In particular, fully homomorphic encryption (FHE) must be employed
because of the complexity of the mathematics involved.

In the introduction we mentioned computation in the cloud as a
motivation for the use of HE.
In this episode we will perform some mock exercises where we "send"
our data to the "cloud" for computing, then get the result back.


## Warm-up: Secure doubling

In your hands-on directory (`Paillier/`) there are three
sample scripts for you to experiment with.
These scripts perform something very silly:
(script 1) encrypts three numbers,
(script 2) multiplies the encrypted numbers by a factor of two, and
(script 3) decrypts the results and prints them to the terminal.
Please inspect them with text editor to understand what is being done.

* `encrypt_phe.py` creates a list of three numbers, encrypts them,
  and saves them to disk (as a surrogate of "sending them to the cloud").
  Note that we use a previously saved public key (`phe_key.pub`) here.

* `math1_phe.py` reads the encrypted numbers, multiplies each number
  by two, then writes them back to another file.
  This represent the mathematical operations done in the cloud on the
  encrypted number.
  The results are then returned to the to the sender.

* `decrypt_phe.py` takes the file "returned" from the cloud and
  decrypts the contents.

You will use these scripts as a starting point for the next exercise.

> ## Running python scripts
>
> The python scripts can be run in several ways:
>
> 1. At the bash prompt, type, e.g., `python encrypt_phe.py`.
>    This will require that you exit the current ipython session.
>    (You can also *suspend* ipython using <kbd>Ctrl</kbd>+<kbd>Z</kbd>
>    and resume it using the `fg` built-in shell command.)
>
> 2. Remember that ipython prompt also doubles as a shell for us!
>    Prefixing the shell command with an exclamation mark (`!`), we can
>    run any command as if we were in a real UNIX shell.
>    Example session:
>
>        In[123]: ! python decrypt_phe.py
>        Decrypted numbers:
>        [6.283185306, 600.0, -9.2e-12]
>
{: .callout}

> ## Which key for what operation?
>
> Look at the scripts closely; what keys are used for what operation?
>
>> ## Solution
>> The public key is used for both encryption and computation phases;
>> the decryption uses the private key.
>> The "cloud" never sees the private key.
>{: .solution}
{: .challenge}

> ## Taking the sum
>
> Now modify the cloud script, `math1_phe.py`, so that it will take the
> sum of all the encrypted numbers it receives, instead of multiplying each
> by a factor of 2.
{: .challenge}


## King Oofy's Wealthy Citizen's Census

Let us imagine that all of us are wealthy persons in the Country of
Oofy, whose assets are in the billions of dollars; yet each one is
reluctant to reveal his/her own asset values and ages.
We are afraid that the King of Oofy would levy us more taxes than we
already bear.
One day, the King of Oofy decreed that all rich folks in the country
must register their net worth so that he could boast of his government's
success everywhere.
He wanted to prove to everyone that under his rule, many young men and
women have achieved status of wealth.
He commissioned his most trusted steward, Ali Wonder, to tally the assets
of all his rich citizens and calculate the average age of these people.
To get around the issue of trust and liability as a result of
collecting these sensitive information bits, Ali Wonder employed
homomorphic encryption (HE) so that he would not even know the
individual specifics of any person.
Ali Wonder assured these rich men and women that, with this HE technology,
no one can ever find out each person's most kept secrets, but the king will
still get his desired stats:

1. total asset value of his country

2. average age of the wealthy people in his country

**Here's how to do this:**

1. King of Oofy created a Paillier keypair, and he withheld the private
   key to himself.

2. He shared the public key to Ali Wonder, who would distribute it to
   all the wealthy citizens in his country.

3. These citizens must submit their individual asset values and ages in the
   encrypted form to Ali Wonder.

4. Ali Wonder, who does not have access to the private key, will sum all the
   asset values and average all the ages in the encrypted form,
   and e-mail the final results, *still in the encrypted form*, to the King.
   The King does not have access to the server and infrastructure where
   Ali Wonder performs his duty.

5. The King, who has the private key to unlock the final answer, will
   reveal these highly-prized statistics at the next feast he will hold
   for his most respected friends and statespersons.


### Privacy-Preserving, Multiparty Computation

The king's computation requires three parties to perform separate computations:

1. Each citizen (us) must come up with our wealth value and age
   and encrypt them, then "send" the encrypted to Ali the steward.

2. Ali must gather all the encrypted data and performs the summation
   (and averaging) on the data.
   At the end, Ali will "send" the final encryted result to King of Offy.

3. The King will use his private key to reveal the final results in plaintext.

> ## Part 1: Citizen's challenge
>
> *(For this hands-on, please create reasonable values the required
> quantities below.)*
>
> Each citizen, hearken the King's decree!
> Please furnish and encrypt (1) the total value of thy assets and
> (2) thy present age.
> The King has shared his public key in thy own working directory, called
> `King-Oofy.pub`.
> Use that key to encrypt thy most guarded secrets.
> Save these encrypted values on a single JSON file at the following path name:
> `/scratch-lustre/DeapSECURE/module05/Oofy/citizens/data-MIDASID.crypt`,
> where thou will substitute thy own MIDAS ID for the `MIDASID` above.
>
>> ## Hints
>>
>> Copy the `encrypt_phe.py` file to `citizen.py`, then edit `citizen.py`.
>> The first number to encrypt will be the total assert value, and the second
>> number the age.
>{: .solution}
>
> IMPORTANT: You need to do one more step to make sure that the
> encrypted data is readable by other people.
> Please insert the following snippet (and replace the `MIDASID` below)
> right after you call the `write_file` function:
>
> ```python
> import os
> os.chmod("/scratch-lustre/DeapSECURE/module05/Oofy/citizens/data-MIDASID.crypt", 0o644)
> ```
{:.challenge}


> ## Part 2: Ali Wonder's challenge
>
> One of the workshop participant will play the Ali Wonder role.
> Ali will scan the `/scratch-lustre/DeapSECURE/module05/Oofy/citizens`
> for all the `data-*.crypt` files.
> His script will open all these files and read the encrypted numbers,
> then process the computation.
> Save the encrypted values of the *sum total of citizen's asset values*
> and the *average of citizen's ages* into
> `/scratch-lustre/DeapSECURE/module05/Oofy/king/census.crypt`.
>
>> ## Hints
>>
>> 1. Use the `glob` function in the `glob` module to get all the names
>>    of the input files:
>>
>>    ```python
>>    from glob import glob
>>    ...
>>    files = glob("/scratch-lustre/DeapSECURE/module05/Oofy/citizens/data-*.crypt")
>>    ```
>>
>> 2. Iterate over all the files using the `for` statement:
>>
>>    ```python
>>    for filename in files:
>>        # ... fill in here
>>    ```
>>
>> 3. Remember that you have to take the sum of all the asset values, and
>>    take the average of all the ages.
>>
>> 4. You can still use the King's public key, `King-Oofy.pub`.
>{: .solution}
>
> IMPORTANT: You also need to employ the `os.chmod(..., 0o644)` described
> in the Citizen's challenge above to make the result world-readable.
>
{:.challenge}


> ## Part 3: King's challenge
>
> The king's challenge is very easy.
> His program will need to read Ali Wonder's total statistics and decrypt it.
>
>> ## Hints
>>
>> Copy the `decrypt_phe.py` file to `king.py` for further modification.
>> Remember that the King's input file will be:
>>
>>     /scratch-lustre/DeapSECURE/module05/Oofy/king/census.crypt
>{: .solution}
{:.challenge}

To keep the anticipation high, the King will reveal the answers
at the end of the workshop!


> ## Advanced: Designing a more secure census
>
> Is it possible to design something more secure than what devised here?
> In the abovementioned scheme,
> you can imagine that the King of Oofy may eventually force Ali Wonder
> to hand off all the encrypted individual data to him for inspection.
>
> One approach is to "split up" the private key and disperse the parts of the
> private keys to the citizens such that the unlocking of
> any ciphertext would require many parties to apply their part of the private
> key to the ciphertext, until the ciphertext is entirely decoded.
> See for example, [Shamir's Secret Sharing](
>     https://en.wikipedia.org/wiki/Shamir's_Secret_Sharing
> ).
> This is well beyond the scope of this workshop, therefore we will not discuss
> it further.
> The point is there are ways to make this process more secure.
>
{:.challenge}


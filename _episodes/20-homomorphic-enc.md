---
title: "Homomorphic Encryption --- A Primer"
teaching: 0
exercises: 0
questions:
- "What is homomorphic encryption (HE)?"
- "What can be encrypted and processed using HE?"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "First key point. Brief Answer to questions. (FIXME)"
---

## Introduction

Homomorphic encryption (HE) is a holy grail in cybersecurity today.
The goal is to protect the sensitive information bits
during the transmission and processing phase without
ever decrypting them for processing.

The term "homomorphic" 

In algebra, a homomorphism is a structure-preserving map between two algebraic structures of the same type (such as two groups, two rings, or two vector spaces).



HO is still an active research area.
However, there are some software libraries available to be used today
(see a [supplementary episode](
    {{ page.root }}{% link _episodes/29-he-implementations.md %}
) for more information).
One big challenge with HE is that its computation cost is very high.
Therefore it makes sense to use HPC if we have to process a lot of numbers
involving HE.

Depending on the encryption scheme, HE would allow different
arithmetic operations to be performed on the encrypted number.
 

## Partially Homomorphic Encryption


## Fully Homomorphic Encryption


## Implementation of Homomorphic Encryption

The following is an incomplete list of software libraries (toolboxes)
that can be used to perform computation with HE.

* [Microsoft SEAL](
  )
  The implementation is available in C++ and 

  * Github resources: 